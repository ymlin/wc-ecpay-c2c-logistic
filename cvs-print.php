<?php

// ini_set('display_errors', 1);
// error_reporting(-1);

if ($_REQUEST) {
	require_once ( '../../../wp-load.php' );
	global $ecpay_cvs;

	$data = $ecpay_cvs->get_ecpay_data( $_REQUEST['id'] );

	$type = $_REQUEST['type'];

	// print_r($data);

	// 7-ELEVEN
	if ($data->LogisticsSubType == 'UNIMARTC2C') {
		$action_url = ($data->MerchantID == '2000933')
			? 'https://logistics-stage.ecpay.com.tw/Express/PrintUniMartC2COrderInfo'
			: 'https://logistics.ecpay.com.tw/Express/PrintUniMartC2COrderInfo';
		
		$post_data = array(
			'AllPayLogisticsID' => $data->AllPayLogisticsID,
			'CVSPaymentNo' => $data->CVSPaymentNo,
			'CVSValidationNo' => $data->CVSValidationNo,
			'MerchantID' => $data->MerchantID,
			'LogisticsSubType' => $data->LogisticsSubType,
			'PlatformID' => '',
		);
		$post_data['CheckMacValue'] = $ecpay_cvs->create_check_code( $post_data );
		unset($post_data['LogisticsSubType']);				
	}
	// 全家
	if ($data->LogisticsSubType == 'FAMIC2C') {
		$action_url = ($data->MerchantID == '2000933')
			? 'https://logistics-stage.ecpay.com.tw/Express/PrintFAMIC2COrderInfo'
			: 'https://logistics.ecpay.com.tw/Express/PrintFAMIC2COrderInfo';

		$post_data = array(
			'AllPayLogisticsID' => $data->AllPayLogisticsID,
			'CVSPaymentNo' => $data->CVSPaymentNo,
			'MerchantID' => $data->MerchantID,
			'LogisticsSubType' => $data->LogisticsSubType,
			'PlatformID' => '',
		);
		$post_data['CheckMacValue'] = $ecpay_cvs->create_check_code( $post_data );
		unset($post_data['LogisticsSubType']);
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>列印寄件單</title>
    <style type="text/css">
        h2 {
            margin: 200px auto 20px;
            text-align: center;
        }
        div.notice {
            border: 5px solid #ccc;
            background-color: #efefef;
            padding: 20px;
            margin: 0 auto;
            width: 630px;
            text-align: center;
        }
    </style>	
	<script>
	function submitForm() {
		document.forms['ecpay_form'].submit();
	}
	</script>
</head>
<body onload="submitForm();">
<div class="notice">
	請稍候!正在處理中...
	<form name="ecpay_form" action="<?php echo $action_url?>" method="POST">
		<?php foreach($post_data as $name => $val):?>
		<input type="hidden" name="<?php echo $name?>" value="<?php echo $val?>">
		<?php endforeach?>
		如果頁面沒有重新導向到列印頁面,請<input type="submit" value="按此列印">
	</form>
</div>
</body>
</html>
