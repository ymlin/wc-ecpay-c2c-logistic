<?php

if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	require_once ( '../../../wp-load.php' );
	
	global $allpay_cvs;

	$post = $_POST;	

	ksort( $post );

	// 訂單處理中(已收到訂單資料) 300 
	if ( $post['RtnCode'] == '300' AND $allpay_cvs->check_value( $post ) ) {
		$order_id = $post['MerchantTradeNo'];

		$order = new WC_Order( $order_id );	

		if ( get_post_meta( $order_id, 'ecpay_logistics', true ) == '' ) {		
			$note = sprintf('物流編號:%s ',$post['AllPayLogisticsID'] );	
			$order->add_order_note( $note, true );	
			add_post_meta( $order_id, 'allpay_logistics', json_encode($post) );	
		}	
	}	
	// 商品已送至物流中心 2030
	if ( $post['RtnCode'] == '2030' AND $allpay_cvs->check_value( $post ) ) {
		$order_id = $post['MerchantTradeNo'];
		$order = new WC_Order( $order_id );
		$order->add_order_note( '商品已送至物流中心' );
	}
	// 門市店 EC 代收(C2C 成功取件) 2067
	if ( $post['RtnCode'] == '2067' AND $allpay_cvs->check_value( $post ) ) {
		$order_id = $post['MerchantTradeNo'];
		$order = new WC_Order( $order_id );
		// $order->update_status( 'completed', 'C2C 成功取件' );
		$order->add_order_note( 'C2C 成功取件' );
	}
}

