<?php

class Wc_Shipping_Ecpay_Cvs
{
	private static $_instance = null;
	private $methods = array();
	public $map_url;

	public static function forge()
	{
		if (is_null(self::$_instance)) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}

	private function __construct()
	{
		// 電子地圖
		$this->map_url = plugins_url( 'cvs-map.php', __FILE__ );

		register_activation_hook( __FILE__, array( $this, 'plugin_activated' ) );
		add_action( 'woocommerce_shipping_init', array( $this, 'init_shipping_ecpay_cvs' ) );		

		if (is_admin()) {
			//add_action( 'add_meta_boxes', array( $this, 'add_print_button' ) );
			 add_filter('manage_edit-shop_order_columns', array( $this, 'allpay_e_cvs_add_column' ) );
			 add_action('manage_shop_order_posts_custom_column' ,array( $this, 'allpay_e_cvs_add_column_action') );
			//由後台更新訂單狀待完成時處理
			// add_action( 'woocommerce_order_status_completed', array( $this, 'run_package_request' ), 1);
		}
	}
	public	function allpay_e_cvs_add_column($columns ){
		$columns['allpay_e_cvs_column']="超商物流單號";
		return $columns;
	}
	public function allpay_e_cvs_add_column_action( $column )
	{
		if($column=='allpay_e_cvs_column'){
		   global $post;

				$order = new WC_Order( $post->ID );

				$shipping = $order->get_shipping_methods();
				$shipping = array_pop($shipping);
				$shipping_method_id = $shipping['method_id'];

				if ($shipping_method_id == 'cvs_unimart_shipping'
					OR $shipping_method_id == 'cvs_fami_shipping'
				) {
					global $post;
					$print_url = plugins_url( 'cvs-print.php?id=', __FILE__ );
					$process_url = plugins_url( 'cvs-process.php?id=', __FILE__ );
					?>
					<?php if ($this->get_ecpay_data()):?>
					<button class="button-primary ecpay_print" type="button" >列印寄件單</button>
					<script>
					jQuery('.ecpay_print').click(function() {
						var id = jQuery(this).parents('tr').attr('id');
						id = id.replace('post-', '');
						window.open('<?php echo $print_url?>'+id, '_blank');
					});
					</script>
					<?php else:?>
					<button class="button-primary ecpay_process" type="button" >產生物流訂單</button>
					<script>
					jQuery('.ecpay_process').click(function() {
						var id = jQuery(this).parents('tr').attr('id');
						id = id.replace('post-', '');
						window.open('<?php echo $process_url?>'+id, 'ecpay', 'width=640,height=480');
					});
					</script>		
					<?php endif;
				}	
						}
	}
	public function plugin_activated()
	{
		if ( !function_exists( 'curl_init' ) ) {
	        deactivate_plugins( basename( __FILE__ ) );
	        wp_die( '抱歉！目前無法啟用這個外掛，請確認PHP環境是否載入curl，<a href="javascript:history.go(-1);">返回</a>。' );
		}		
	}

	public function init_shipping_ecpay_cvs()
	{
		if ( ! class_exists( 'Shipping_Ecpay_Cvs') ) {
			require_once( dirname(__FILE__) . '/lib/shipping-ecpay-cvs.php' );
		}

		// unset( WC()->session->chosen_shipping_methods );

		$gateway_path = plugin_dir_path( __FILE__ ) . 'shipping/';

		$classes = glob( $gateway_path . 'wc-shipping-cvs-*.php' );

		if ( ! $classes ) return;

		foreach ( $classes as $class ) {
			$class_name =  basename( str_replace( '-', '_', $class), '.php');
			if ( ! class_exists( $class_name ) ) {
				require_once( $class );
				if ( version_compare( get_woo_version(), '2.6.0' ) == 1 ) {
					$name = str_replace( 'wc_shipping_', '', $class_name ) . '_shipping';
					$this->methods[$name] = $class_name;
				} else {
					$this->methods[] = $class_name;
				}
			}			
		}
		add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
		add_filter( 'woocommerce_shipping_methods', array( $this, 'add_ecpay_cvs_shipping' ) );
		
		// Alan 檢查是否選了門市
		add_action( 'woocommerce_after_checkout_validation', array( $this, 'validate_map_result' ) );	
		// Alan 前台存user選的門市資料
		add_action( 'woocommerce_checkout_order_processed', array( $this, 'save_map_result') );
		
		// Alan 改由後台更新訂單狀待完成時處理 line 39
		// add_action( 'woocommerce_checkout_order_processed', array( $this, 'run_package_request' ) );		
		
		// add_action( 'woocommerce_checkout_process', array( $this, 'validate_phone_number' ) );

		add_action( 'woocommerce_checkout_order_review', array( $this, 'store_info' ) );
		add_action( 'wp_footer', array( $this, 'footer_script' ) );
	}

	public function add_ecpay_cvs_shipping( $methods )
	{
		$methods = array_merge( $methods, $this->methods );
		return $methods;		
	}

	public function get_ecpay_data( $id = '' )
	{
		if (empty($id)) {
			global $post;
			$id = $post->ID;
		}
		$data = get_post_meta( $id, 'ecpay_logistics', true);
		$data = json_decode($data);		
		
		return $data;
	}

	public function add_print_button()
	{
		// if (!$this->get_ecpay_data()) return;

		global $post;

		$order = new WC_Order( $post->ID );

		$shipping = $order->get_shipping_methods();
		$shipping = array_pop($shipping);
		$shipping_method_id = $shipping['method_id'];

		if ($shipping_method_id == 'cvs_unimart_shipping'
			OR $shipping_method_id == 'cvs_fami_shipping'
		) {
			add_meta_box(
		        'woocommerce-order-ecpay-button',
		        __( '綠界C2C超商取貨', 'woocommerce' ),
		        array( $this, 'order_meta_box_print_button' ),
		        'shop_order',
		        'side',
		        'default'
		    );	
		}	
	}

	public function order_meta_box_print_button()
	{
		global $post;
		$print_url = plugins_url( 'cvs-print.php?id='.$post->ID, __FILE__ );
		$process_url = plugins_url( 'cvs-process.php?id='.$post->ID, __FILE__ );
		?>
		<?php if ($this->get_ecpay_data()):?>
		<button class="button-primary" type="button" id="ecpay_print">列印寄件單</button>
		<script>
		jQuery('#ecpay_print').click(function() {
			window.open('<?php echo $print_url?>', '_blank');
		});
		</script>
		<?php else:?>
		<button class="button-primary" type="button" id="ecpay_process">產生物流訂單</button>
		<script>
		jQuery('#ecpay_process').click(function() {
			window.open('<?php echo $process_url?>', 'ecpay', 'width=640,height=480');
		});
		</script>		
		<?php endif;		
	}

	// 加入選取門市按鈕
	public function footer_script()
	{
		$reset_url = plugins_url( 'cvs-reset.php', __FILE__ );
		?>
		<script>
		jQuery(document).ready(function($) {

			$('.store-block').hide();	
			$('.store-info').hide();

		    function toggleCustomBox() {	        
		        var selectedMethod = $('input:checked', '#shipping_method').attr('id');

		        $.get('<?php echo $reset_url?>', function(resp) {
		        	$('.store-info').hide();
		        },'json');


				$('.store-block').fadeOut();

		        if (selectedMethod === 'shipping_method_0_cvs_unimart_shipping'
		        	|| selectedMethod === 'shipping_method_0_cvs_fami_shipping'
	        	) {
		            setTimeout(function(){
		            	$('.store-block').fadeIn();        
		           	}, 1000);
		        } else {
		            $('.store-block').hide();
		        }
		    }

		    $(document).ready(toggleCustomBox);

		    $(document).on('change', '#shipping_method input:radio', toggleCustomBox);

		    $('.custom-button').click(function() {
		    	window.open( '<?php echo $this->map_url?>', '_blank' );
		    });

			<?php if ( WC()->session->get('ecpay_map_result') ):?>
			$('.store-info').show();
			<?php endif?> 
		});
		</script>
		<?php
	}

	public function store_info( $order_id )
	{
		$button_text = '選取超商門市';
		$map_result = WC()->session->get('ecpay_map_result');
		?>
		<div class="store-block">
			<input type="button" class="custom-button" value="<?php echo $button_text?>">
			<div class="store-info" style="padding-top:20px;">
				<label>門市名稱：</label>
				<span id="CVSStoreName">
					<?php echo (isset($map_result['CVSStoreName']))?$map_result['CVSStoreName']:''?>
				</span>
				<br>
				<label>門市地址：</label>
				<span id="CVSAddress">
					<?php echo (isset($map_result['CVSAddress']))?$map_result['CVSAddress']:''?>				
				</span>
				<br>
				<label id="telephone">門市電話：</label>
				<span id="CVSTelephone">
					<?php echo (isset($map_result['CVSTelephone']))?$map_result['CVSTelephone']:''?>					
				</span>
			</div>
			<hr>
		</div>
		<?php
	}

	public function create_check_code( $post, $settings = '') 
	{
		if (empty($settings)) {
			$shipping = ($post['LogisticsSubType'] == 'FAMIC2C')
				? 'cvs_fami_shipping'
				: 'cvs_unimart_shipping';

			if (version_compare(get_woo_version(), '2.6.0') == 1) {
				$settings = get_shipping_option($shipping);
			} else {
				$shipping_setting = 'woocommerce_'.$shipping.'_settings';		
				$settings = get_option( $shipping_setting );
			}
			unset($post['LogisticsSubType']);
		}	

		if (isset($post['CheckMacValue'])) {
			unset($post['CheckMacValue']);
		}

		$hash_raw_data = 'HashKey='.$settings['ecpay_hash_key'].'&'.urldecode(http_build_query($post)).'&HashIV='.$settings['ecpay_hash_iv'];
		$urlencode_data = strtolower(urlencode($hash_raw_data));

		$urlencode_data = str_replace('%2d', '-', $urlencode_data);
		$urlencode_data = str_replace('%5f', '_', $urlencode_data);
		$urlencode_data = str_replace('%2e', '.', $urlencode_data);
		$urlencode_data = str_replace('%21', '!', $urlencode_data);
		$urlencode_data = str_replace('%2a', '*', $urlencode_data);
		$urlencode_data = str_replace('%28', '(', $urlencode_data);
		$urlencode_data = str_replace('%29', ')', $urlencode_data);
		// exit($urlencode_data); 
		return strtoupper(md5($urlencode_data));		
	}

	public function check_value( $post ) 
	{
		$shipping = ($post['LogisticsSubType'] == 'FAMIC2C')
			? 'cvs_fami_shipping'
			: 'cvs_unimart_shipping';

		if (version_compare(get_woo_version(), '2.6.0') == 1) {
			$settings = get_shipping_option($shipping);
		} else {
			$shipping_setting = 'woocommerce_'.$shipping.'_settings';		
			$settings = get_option( $shipping_setting );
		}

		$MyCheckMacValue = $this->create_check_code( $post, $settings );
			
		if ( $MyCheckMacValue == $post['CheckMacValue'] ) {
			return true;
		}
		return false;
	}	

	public function validate_map_result()
	{
		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
		$chosen_shipping = $chosen_methods[0];

		if ($chosen_shipping != 'cvs_unimart_shipping' AND $chosen_shipping != 'cvs_fami_shipping') return;

		$map_result = WC()->session->get('ecpay_map_result');

		if (!$map_result) {
			throw new Exception('請選擇取貨超商門市');
		}		

		// 檢查手機號碼
		$phone = (isset( $_POST['billing_phone'] ) ? trim( $_POST['billing_phone'] ) : '');
		if ( ! preg_match( "/^[0][1-9]{1,3}[0-9]{6,8}$/", $phone ) ) {
			throw new Exception( '聯絡電話只接受數字格式，例:手機0988888888或市話0288888888' );
		}		
	}		

	// 檢查手機號碼
	// public function validate_phone_number() 
	// {
	// 	$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
	// 	$chosen_shipping = $chosen_methods[0];

	// 	if ($chosen_shipping != 'cvs_unimart_shipping' AND $chosen_shipping != 'cvs_fami_shipping') return;

	// 	$phone = (isset( $_POST['billing_phone'] ) ? trim( $_POST['billing_phone'] ) : '');
	// 	if ( ! preg_match( "/^[0][1-9]{1,3}[0-9]{6,8}$/", $phone ) ) {
	// 		throw new Exception( '聯絡電話請輸入正確手機號碼,格式為0988888888' );
	// 	}	
	// }

	// 儲存map資料
	public function save_map_result( $order_id )
	{
		$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
		$chosen_shipping = $chosen_methods[0];

		if ( $chosen_shipping != 'cvs_unimart_shipping' AND $chosen_shipping != 'cvs_fami_shipping' ) return;

		$map_result = WC()->session->get('ecpay_map_result');

		add_post_meta( $order_id, 'ecpay_map_result', json_encode($map_result) );	
	}

	// 處理歐付寶物流資料傳送
	public function run_package_request( $order_id )
	{		
		$map_result = get_post_meta( $order_id, 'ecpay_map_result', true );

		if (!$map_result) return;

		$map_result = json_decode( $map_result, true );

		$order = new WC_Order( $order_id );
		$total = $order->get_total();
		$shipping_cost = $order->get_total_shipping();
		$amount = $total - $shipping_cost;
		$payment_method = get_post_meta( $order_id, '_payment_method', true );

		$shipping_items = $order->get_shipping_methods();
		$shipping_items = array_pop($shipping_items);

		$chosen_shipping = $shipping_items['method_id'];

		if ($chosen_shipping != 'cvs_unimart_shipping' AND $chosen_shipping != 'cvs_fami_shipping') return;

		$is_collection = 'N';

		switch ($payment_method) {
			case 'ecpay_cvs_unimart_pay':
			case 'ecpay_cvs_fami_pay':
				$is_collection = 'Y';
				break;
		}

		if ( version_compare( get_woo_version(), '2.6.0' ) == 1 ) {
			$settings = get_shipping_option( $map_result['ExtraData'] );
		} else {
			$shipping_setting = 'woocommerce_'.$map_result['ExtraData'].'_settings';
			$settings = get_option( $shipping_setting );
		}

		$ecpay_url = ($settings['ecpay_merchant_id'] == '2000933')
			? 'https://logistics-stage.ecpay.com.tw/Express/Create'
			: 'https://logistics.ecpay.com.tw/Express/Create';
	
		$post_data = array(
			'MerchantID' => $settings['ecpay_merchant_id'],
			'MerchantTradeNo' => $order_id,
			'MerchantTradeDate' => date('Y/m/d H:i:s'),
			'LogisticsType' => 'CVS',
			'LogisticsSubType' => $map_result['LogisticsSubType'],
			//GC_minos:原為$amount，但此金額為扣掉運費之後的。
			'GoodsAmount' => $total,
			'CollectionAmount' => '',
			'IsCollection' => $is_collection,
			'GoodsName' => '網路商品',
			'SenderName' => $settings['ecpay_sender_name'],
			'SenderPhone' => '',
			'SenderCellPhone' => $settings['ecpay_sender_cellphone'],
			'ReceiverName' => $order->billing_last_name.$order->billing_first_name,
			'ReceiverPhone' => '',
			'ReceiverCellPhone' => $order->billing_phone,
			'ReceiverEmail' => $order->billing_email,
			'TradeDesc' => '',
			'ServerReplyURL' => plugins_url( 'cvs-notify.php', __FILE__ ),
			'ClientReplyURL' => plugins_url( 'cvs-process.php', __FILE__ ),
			'LogisticsC2CReplyURL' => plugins_url( 'cvs-notify.php', __FILE__ ),
			'Remark' => '',
			'PlatformID' => '',
			'ReceiverStoreID' => $map_result['CVSStoreID'],
			'ReturnStoreID' => '', // (逆物流)退貨用的商店編號		
		);

		ksort($post_data); // key 由小到大排序

		$post_data['CheckMacValue'] = $this->create_check_code( $post_data, $settings );

		$post_data['ecpay_url'] = $ecpay_url;

		return $post_data;

		// $ch = curl_init(); 
		// curl_setopt($ch, CURLOPT_URL, $ecpay_url); 
		// curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6'); 
		// curl_setopt($ch, CURLOPT_HEADER, 1);		
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);		
		// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 900000);		
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// curl_setopt($ch, CURLOPT_COOKIEJAR, './cookie.txt');
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

		// $response = curl_exec($ch); 
		// $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		// $header = substr($response, 0, $header_size);
		// $body = substr($response, $header_size);

		// $body = substr($response, $header_size);

  //       $tmp = explode('|', $body);
  //       $result = array();
  //       $result['code'] = $tmp[0];

  //       if ($result['code'] == '1') {
  //           $ok = array();
  //           parse_str($tmp[1], $ok);
  //           $result = array_merge($result, $ok);
  //       } else {
  //           $result['error_message'] = $tmp[1];
  //       }

		// if ($result['code'] == '1') {	
		// 	// $note = sprintf('物流訂單編號:%s', $result['LogisticsID'] );		
		// 	// $order->add_order_note($note, true);	
		// } else {
		// 	$order->add_order_note($result['error_message']);
		// }
	}		
}

global $ecpay_cvs;

$ecpay_cvs = Wc_Shipping_Ecpay_Cvs::forge();
