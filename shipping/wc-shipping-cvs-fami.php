<?php




class WC_Shipping_Cvs_Fami extends Shipping_Ecpay_Cvs
{
    public $min_amount;

    public $requires;

    public function __construct($instance_id = 0)
    {
        $this->id                 = 'cvs_fami_shipping';
        $this->method_title       = '超商取貨(全家)';
        $this->method_description = '全家超商(店到店)';
        $this->cost               = $this->get_option('cost');
        $this->availability       = $this->get_option('availability');
        $this->countries          = $this->get_option('countries');
        $this->requires           = $this->get_option('requires');
        $this->free_cost          = $this->get_option('free_cost');
        // $this->free_shipping_items = $this->get_option('free_shipping_items');

        if (version_compare(get_woo_version(), '2.6.0') == 1) {
            $this->instance_id = absint($instance_id);
            $this->supports    = array(
                'shipping-zones',
                'instance-settings',
                'instance-settings-modal',
            );
            $this->enabled = $this->get_option('enabled');
        }

        parent::__construct($instance_id);
        $this->title = $this->get_option('title', $this->method_title);
    }

    public function calculate_shipping($package = array())
    {
        global $woocommerce;

        $cost = ((int) $woocommerce->cart->cart_contents_total >= (int) $this->get_option('free_cost'))
        ? 0
        : $this->get_option('cost');

        $rate = array(
            'id'       => $this->id,
            'label'    => $this->title,
            'cost'     => $cost,
            'calc_tax' => 'per_item',
        );

        // Register the rate
        $this->add_rate($rate);
    }
}
