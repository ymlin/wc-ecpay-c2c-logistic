<?php

/*
Plugin Name: 綠界C2C(超商取貨-門市寄/取)
Description: 綠界C2C物流 For Woocommerce
Version: 1.0
Author: Alan <alanchang15@gmail.com>
 */

$active_plugins = apply_filters('active_plugins', get_option('active_plugins'));

if (in_array('woocommerce/woocommerce.php', $active_plugins)) {
    require_once __DIR__ . '/wc-shipping-ecpay-cvs.php';
    require_once __DIR__ . '/wc-gateway-ecpay-cvs.php';

    if (!function_exists('get_woo_version')) {
        function get_woo_version()
        {
            return WC()->version;
        }
    }

    if (!function_exists('get_shipping_option')) {
        function get_shipping_option($chosen_shipping)
        {
            global $wpdb;
            $settings = array();
            $table    = $wpdb->prefix . 'options';
            $name     = 'woocommerce_' . $chosen_shipping;
            $sql      = "SELECT option_value FROM $table WHERE option_name LIKE '%$name%'";
            $row      = $wpdb->get_row($sql);
            if ($row) {
                $settings = unserialize($row->option_value);
            }

            return $settings;
        }
    }
}
